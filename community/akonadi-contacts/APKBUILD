# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=akonadi-contacts
pkgver=21.12.2
pkgrel=0
pkgdesc="Libraries and daemons to implement Contact Management in Akonadi"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by akonadi
# ppc64le blocked by qt5-qtwebengine -> akonadi
arch="all !armhf !s390x !riscv64 !ppc64le"
url="https://community.kde.org/KDE_PIM"
license="LGPL-2.0-or-later AND GPL-2.0-or-later AND BSD-3-Clause"
depends_dev="
	akonadi-dev>=$pkgver
	gpgme-dev
	grantlee-dev
	grantleetheme-dev
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kcontacts-dev
	kdbusaddons-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kmime-dev
	kservice-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libkleo-dev
	prison-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/akonadi-contacts-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
4a8cfbb3c287d41eefe13c3ada42c5098460cab0b72b5660f0ee02c5d6baecef2c7edd64b2f3789ea6c96c287bf76b329430780310b11836cf8d2e8b66c527b7  akonadi-contacts-21.12.2.tar.xz
"

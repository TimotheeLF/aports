# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kde-dev-utils
pkgver=21.12.2
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kparts-dev
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/development/"
pkgdesc="Small utilities for developers using KDE/Qt libs/frameworks"
license="(LGPL-2.0-only OR LGPL-3.0-only) AND GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcoreaddons-dev
	ki18n-dev
	kparts-dev
	kwidgetsaddons-dev
	qt5-qttools-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kde-dev-utils-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e76dbb6015bbf9c1d8baee38c2766939e04984389004a4d82367611460982b3fd657791a7814c6f3eb3f18ba1804026d446ad2125c13e67f079e2064509ea664  kde-dev-utils-21.12.2.tar.xz
"

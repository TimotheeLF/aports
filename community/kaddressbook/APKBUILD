# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kaddressbook
pkgver=21.12.2
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/components/kaddressbook.html"
pkgdesc="Address Book application to manage your contacts"
license="GPL-2.0-or-later AND LGPL-2.0-or-later"
depends="kdepim-runtime"
makedepends="
	akonadi-dev
	akonadi-search-dev
	extra-cmake-modules
	gpgme-dev
	grantleetheme-dev
	kcmutils-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kontactinterface-dev
	kpimtextedit-dev
	kuserfeedback-dev
	kuserfeedback-dev
	libkdepim-dev
	libkleo-dev
	pimcommon-dev
	prison-dev
	qt5-qtbase-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kaddressbook-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
5d325454e30588cb73e60d37b0438c639fb13e42d77d6cef2c7a1ebecb7b819b9987fb5e15419b5d514ad68bbea8769f9bf9c68d8de88b563fd7a5278aa30d14  kaddressbook-21.12.2.tar.xz
"

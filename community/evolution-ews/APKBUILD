# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=evolution-ews
pkgver=3.42.4
pkgrel=0
pkgdesc="MS Exchange integration through Exchange Web Services"
url="https://wiki.gnome.org/Apps/Evolution/EWS"
arch="all !s390x !ppc64le" # blocked by evolution
license="LGPL-2.1-or-later"
makedepends="cmake gtk-doc intltool glib-dev gettext-dev evolution-data-server-dev
	evolution-dev libmspack-dev uhttpmock-dev samurai"
options="!check" # fail in docker due to port restrictions
subpackages="$pkgname-lang"
source="https://download.gnome.org/sources/evolution-ews/${pkgver%.*}/evolution-ews-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=None \
		-DENABLE_TESTS=TRUE
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
5ce1a228414d8c456962ce944a01efbeefb059bc9ff358c95bb7cec72e74ad29cd9476c9aa396b01df35d7c89fff743837dc2f9763129a5f794cbbd14557c2e3  evolution-ews-3.42.4.tar.xz
"

# Contributor: psykose <alice@ayaya.dev>
# Contributor: Alex Yam <alex@alexyam.com>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=wasi-libcxx
# match llvm version
pkgver=12.0.1
_llvmver="${pkgver%%.*}"
_wasi_sdk_ver=14
pkgrel=1
pkgdesc="WASI LLVM C++ standard library"
subpackages="wasi-libcxxabi:_libcxxabi"
url="https://libcxx.llvm.org/"
arch="all"
license="Apache-2.0 WITH LLVM-exception"
makedepends="
	clang
	cmake
	libxml2-dev
	llvm$_llvmver-dev
	python3-dev
	samurai
	wasi-libc
	zlib-dev
	"
source="https://github.com/llvm/llvm-project/releases/download/llvmorg-$pkgver/libcxx-$pkgver.src.tar.xz
	https://github.com/llvm/llvm-project/releases/download/llvmorg-$pkgver/libcxxabi-$pkgver.src.tar.xz
	https://github.com/llvm/llvm-project/releases/download/llvmorg-$pkgver/llvm-$pkgver.src.tar.xz
	https://github.com/WebAssembly/wasi-sdk/archive/refs/tags/wasi-sdk-$_wasi_sdk_ver.tar.gz
	"
builddir="$srcdir"/wasi-libcxx
# TODO: check, needs to somehow pass wasi sysroot include to lit..
options="!check"

prepare() {
	default_prepare
	mkdir -p "$builddir"
	for s in libcxx libcxxabi llvm; do
		mv "$srcdir"/$s-$pkgver.src "$builddir"/$s
	done
	mv "$srcdir"/wasi-sdk-wasi-sdk-$_wasi_sdk_ver/wasi-sdk.cmake "$builddir"
	mv "$srcdir"/wasi-sdk-wasi-sdk-$_wasi_sdk_ver/cmake "$builddir"
}

build() {
	export CFLAGS="$CFLAGS -fno-exceptions --sysroot=/usr/share/wasi-sysroot"
	export CXXFLAGS="$CXXFLAGS -fno-exceptions --sysroot=/usr/share/wasi-sysroot"

	cmake -B build-libcxx -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_MODULE_PATH="$builddir"/cmake \
		-DCMAKE_TOOLCHAIN_FILE="$builddir"/wasi-sdk.cmake \
		-DCMAKE_C_COMPILER_WORKS=ON \
		-DCMAKE_CXX_COMPILER_WORKS=ON \
		-DCMAKE_STAGING_PREFIX=/usr/share/wasi-sysroot \
		-DLIBCXX_INCLUDE_TESTS=ON \
		-DLIBCXX_ENABLE_EXCEPTIONS=OFF \
		-DLIBCXX_ENABLE_SHARED=OFF \
		-DLIBCXX_ENABLE_THREADS=OFF \
		-DLIBCXX_HAS_PTHREAD_API=OFF \
		-DLIBCXX_HAS_EXTERNAL_THREAD_API=OFF \
		-DLIBCXX_BUILD_EXTERNAL_THREAD_LIBRARY=OFF \
		-DLIBCXX_HAS_WIN32_THREAD_API=OFF \
		-DLIBCXX_ENABLE_EXPERIMENTAL_LIBRARY=OFF \
		-DLIBCXX_ENABLE_FILESYSTEM=OFF \
		-DLIBCXX_CXX_ABI=libcxxabi \
		-DLIBCXX_CXX_ABI_INCLUDE_PATHS=libcxxabi/include \
		-DLIBCXX_HAS_MUSL_LIBC=ON \
		-DLIBCXX_ABI_VERSION=2 \
		-DWASI_SDK_PREFIX=/usr \
		-DLIBCXX_LIBDIR_SUFFIX=/wasm32-wasi \
		libcxx
	cmake --build build-libcxx

	cmake -B build-libcxxabi -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_MODULE_PATH="$builddir"/cmake \
		-DCMAKE_TOOLCHAIN_FILE="$builddir"/wasi-sdk.cmake \
		-DCMAKE_C_COMPILER_WORKS=ON \
		-DCMAKE_CXX_COMPILER_WORKS=ON \
		-DCMAKE_STAGING_PREFIX=/usr/share/wasi-sysroot \
		-DLIBCXXABI_INCLUDE_TESTS=ON \
		-DLIBCXXABI_ENABLE_EXCEPTIONS=OFF \
		-DLIBCXXABI_ENABLE_SHARED=OFF \
		-DLIBCXXABI_ENABLE_THREADS=OFF \
		-DLIBCXXABI_HAS_PTHREAD_API=OFF \
		-DLIBCXXABI_HAS_EXTERNAL_THREAD_API=OFF \
		-DLIBCXXABI_BUILD_EXTERNAL_THREAD_LIBRARY=OFF \
		-DLIBCXXABI_HAS_WIN32_THREAD_API=OFF \
		-DLIBCXXABI_SILENT_TERMINATE:BOOL=ON \
		-DLIBCXXABI_ENABLE_PIC=OFF \
		-DUNIX=ON \
		-DCXX_SUPPORTS_CXX11=ON \
		-DLIBCXXABI_LIBCXX_PATH=libcxx \
		-DLIBCXXABI_LIBCXX_INCLUDES="$builddir"/build-libcxx/include/c++/v1 \
		-DWASI_SDK_PREFIX=/usr \
		-DLIBCXXABI_LIBDIR_SUFFIX=/wasm32-wasi \
		libcxxabi
	cmake --build build-libcxxabi
}

package() {
	DESTDIR="$pkgdir" cmake --install build-libcxx
}

_libcxxabi() {
	pkgdesc="WASI Low level support for the LLVM C++ standard library."
	DESTDIR="$subpkgdir" cmake --install "$builddir"/build-libcxxabi
}

sha512sums="
c9f9a546d6a312ff6e7c85a044ce801fe7bfca1c349767b3f3c5ea16656b8906a8078f25de38138c9844c4b2646238fd17d890438cd10391cd9e4a430f9064a0  libcxx-12.0.1.src.tar.xz
c7b236e9afe8d6bba297e1eca12ca0e03fbd1b028b9627154186d27eb2e035cd33eddb72511a38d0827de347a487e180c32c1798759c4283b9fbbf338a015bb9  libcxxabi-12.0.1.src.tar.xz
ff674afb4c8eea699a4756f1bb463f15098a7fa354c733de83c024f8f0cf238cd5f19ae3ec446831c7109235e293e2bf31d8562567ede163c8ec53af7306ba0f  llvm-12.0.1.src.tar.xz
4fecb3d9c04b91eb2388a9e51d49fbff6f22b81f9945a07ecdbfe479c96dad1e3b673b8bee24842b0dae5294129a9cb35dcf8e5ecf45437a6d01fb6e0fd13645  wasi-sdk-14.tar.gz
"

# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libkdcraw
pkgver=21.12.2
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x blocked by qt5-qtbase-x11
arch="all !armhf !s390x"
url="https://kde.org"
pkgdesc="RAW image file format support for KDE"
license="GPL-2.0-or-later AND LGPL-2.0-or-later"
depends_dev="
	libraw-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkdcraw-$pkgver.tar.xz"
subpackages="$pkgname-dev"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
ea5d630a99b8ac14e3d3fdbb8846b2cf9f9da7fc4416af1dcb4cb5dac77b85aaf268cce31dd6ed3c43f05152b0ebaec81f6898200e1d472b6377f69f6bca9825  libkdcraw-21.12.2.tar.xz
"

# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=mailcommon
pkgver=21.12.2
pkgrel=0
pkgdesc="KDE PIM library providing support for mail applications"
# riscv64 disabled due to missing rust in recursive dependency
arch="all !ppc64le !s390x !armhf !riscv64" # Limited by messagelib -> qt5-qtwebengine
url="https://kontact.kde.org/"
license="GPL-2.0-or-later"
# TODO: Consider replacing gnupg with specific gnupg subpackages that mailcommon really needs.
depends="gnupg"
depends_dev="
	akonadi-dev
	akonadi-mime-dev
	karchive-dev
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kdbusaddons-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kitemmodels-dev
	kitemviews-dev
	kmailtransport-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	libkdepim-dev
	mailimporter-dev
	messagelib-dev
	qt5-qtbase-dev
	qt5-qttools-dev
	syntax-highlighting-dev
	"
makedepends="$depends_dev extra-cmake-modules"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/mailcommon-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"
options="!check" # Requires running dbus server

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
ce435cad63436b4c3220baff75f23ab6e7f216fe439e5667a268ee1236cb59e55f90627afc4f1a83b1ea5121de4e0d18ffa50616b414ad9aa4e53fa13a4e8ba0  mailcommon-21.12.2.tar.xz
"

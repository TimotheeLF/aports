# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=krunner
pkgver=5.91.0
pkgrel=0
pkgdesc="Framework for providing different actions given a string query"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit
arch="all !armhf !s390x !riscv64"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only AND LGPL-2.1-or-later"
depends_dev="
	kconfig-dev
	kcoreaddons-dev
	ki18n-dev
	kio-dev
	kservice-dev
	plasma-framework-dev
	plasma-framework-dev
	qt5-qtbase-dev
	threadweaver-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/krunner-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"
options="!check" # Requires running dbus instance

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
ed00401417526f67d5d27033e1f93fd52485530dcb3e60fbf367fe22956002ab38d5786661107225417a985e15cfc17acc5ef29181104c90059b047e47f3f69b  krunner-5.91.0.tar.xz
"

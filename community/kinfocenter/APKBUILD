# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kinfocenter
pkgver=5.24.2
pkgrel=0
pkgdesc="A utility that provides information about a computer system"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://kde.org/plasma-desktop/"
license="(GPL-2.0-only OR GPL-3.0-only) AND (LGPL-2.1-only OR LGPL-3.0-only)"
makedepends="
	extra-cmake-modules
	glu-dev
	kcmutils-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdeclarative-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kpackage-dev
	kservice-dev
	kwayland-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	qt5-qtbase-dev
	solid-dev
	solid-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kinfocenter-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
5e254043b1be11a65da88e1db22c938d40e534881843b6b5e119e4026b0fb6b23903b6d5e9a64e17d350c644754d4a702ca206424085ed269f462cf2dcf431ee  kinfocenter-5.24.2.tar.xz
"

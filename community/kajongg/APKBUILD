# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kajongg
pkgver=21.12.2
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kconfigwidgets
arch="noarch !armhf !s390x !riscv64"
url="https://kde.org/applications/games/org.kde.kajongg"
pkgdesc="Mah Jongg - the ancient Chinese board game for 4 players"
license="GPL-2.0-or-later AND GFDL-1.2-only"
depends="
	python3
	py3-twisted
	"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	kdoctools-dev
	libkmahjongg-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kajongg-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e5014c7cb0073dd8d6f8db18fe28a3b018f579cb9da22d3cffdcf68387bcb56d45c47843598ffd1b387df63bcef0c87db47e8bc13cd6215a7ebed4cc45e7dd9d  kajongg-21.12.2.tar.xz
"
